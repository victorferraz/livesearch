##Live Search

###Bibliotecas javascript, utilizadas:
- RequireJS
- Jquery

###Dependências:
- Node
- Npm
- Grunt

###Instruções para instação automática:
make config

###Instruções para instalação global: ( Node, Npm e Grunt )
- brew install node;
- sudo curl -O https://www.npmjs.org/install.sh; 
- sudo sh install.sh
- npm install -g grunt-cli

###Instruções para instalação local: (npm install, bower install)
- npm install
- bower install

###Instruções para rodar o projeto otimizado:
- grunt serve:dist

###Instruções para rodar o projeto sem estar otimizado:
- grunt serve





