'use strict';

define(['jquery', 'exports', 'livesearch/delay', 'livesearch/normalizeValue'], function($, exports, delay, normalizeValue) {

    var Highlight = function () {
        this.resultHighlight = $('#result-highlight');
        this.highlightResult = [];
    };

    var templateHighlight = '';
    templateHighlight += '<li class="highligh-list">';
    templateHighlight += '<a href="${url}">';
    templateHighlight += '<img class="highlight-image" src="${logo}" >';
    templateHighlight += '<span class="result-text">${title}</span>';
    templateHighlight += '</a>';
    templateHighlight += '</li>';

    Highlight.prototype.searchHighlights = function (obj) {
        this.currentVal = obj.currentVal;
        var hightlights = obj.response.hightlights;
        var length = hightlights.length;
        for (var i = 0; i < length; i++) {
            this.findHilights(hightlights[i]);
        }
        return this.iterateHighlight();
    };

    Highlight.prototype.findHilights = function (item) {
        var length = item.queries.length;
        var currentItem = null;
        var itemArray = [];
        var count = 0;
        for (var i = 0; i < length; i++) {
            itemArray = [];
            currentItem = normalizeValue(item.queries[i]);
            if (currentItem.indexOf(this.currentVal) > -1 && count === 0){
                count++;
                itemArray.jsonVal = item.queries[i];
                itemArray.highlight = item;
                this.highlightResult.push(itemArray);
            }
        }
    };

    Highlight.prototype.iterateHighlight = function () {
        var list = '';
        var length = this.highlightResult.length;
        for (var i = 0; i < length; i++) {
            list += this.setTemplateHighlight(this.highlightResult[i]);
        }
        this.resultHighlight.html(list);
    };

    Highlight.prototype.setTemplateHighlight = function(item) {
        return templateHighlight.split('${logo}').join(item.highlight.logo)
            .split('${title}').join(item.highlight.title)
            .split('${url}').join(item.highlight.url);
    };


    exports.Highlight = Highlight;
});
