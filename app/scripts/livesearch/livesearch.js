'use strict';

define(['jquery', 'exports', 'livesearch/delay', 'livesearch/normalizeValue', 'livesearch/suggestion', 'livesearch/highlight' ], function($, exports, delay, normalizeValue, suggestion, highlight) {

    var Livesearch = function (selector) {
        this.inputSearch = selector;
        this.suggestionBox = $('#suggestion');
        this.Suggestion = new suggestion.Suggestion();
        this.Highlight = new highlight.Highlight();
        this.currentVal = null;
        this.delay = delay;
        this.resultHighlight = $('#result-highlight');
        this.resultSuggestion = $('#result-suggestion');
        this.doc = $(document);
        this.busqueG1 = $('#busque-g1');
        this.init();
    };

    Livesearch.prototype.init = function () {
        this.addEventListeners();
    };

    Livesearch.prototype.addEventListeners = function () {
        this.inputSearch.keyup($.proxy(this.run, this));
        this.doc.click($.proxy(this.clearSearch, this));
    };

    Livesearch.prototype.run = function (e) {
        this.Highlight.highlightResult = [];
        this.Suggestion.suggestionResult = [];
        this.currentVal = normalizeValue($(e.currentTarget).val());
        this.delay($.proxy(this.getJson, this, e), 300);
    };

    Livesearch.prototype.getJson = function (e) {
        if (this.currentVal.length >1 && e.which !==0){

            this.busqueG1.attr('href', 'http://g1.globo.com/busca/?q='+this.currentVal);
            this.suggestionBox.addClass('open');
            var json = '/data/data.json';
            $.getJSON(json).
                done($.proxy(this.getSuccess, this)).
                fail($.proxy(this.getError, this));

        }else{
            this.clearSearch();
        }
    };

    Livesearch.prototype.getSuccess = function (response) {
        var obj = {'response': response, 'currentVal': this.currentVal};
        this.Suggestion.searchSuggestion(obj);
        this.Highlight.searchHighlights(obj);
    };

    Livesearch.prototype.clearSearch = function (e) {
        var id = '';
        if (e) {
            id = e.target.id;
        }
        if (id !== this.suggestionBox){
            this.suggestionBox.removeClass('open');
            this.resultSuggestion.html('');
            this.resultHighlight.html('');
        }
    };

    exports.Livesearch = Livesearch;

});
