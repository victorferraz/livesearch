'use strict';

define(['jquery', 'exports', 'livesearch/delay', 'livesearch/normalizeValue'], function($, exports, delay, normalizeValue) {

    var Suggestion = function () {
        this.resultSuggestion = $('#result-suggestion');
        this.suggestionResult = [];
    };

    var templateSuggestion = '';
    templateSuggestion += '<li class="suggestion-list">';
    templateSuggestion += '<a href="http://g1.globo.com/busca/?q=${link}">';
    templateSuggestion += '<span class="result-text">${title}</span>';
    templateSuggestion += '</a>';
    templateSuggestion += '</li>';

    Suggestion.prototype.searchSuggestion = function (obj) {

        this.currentVal = obj.currentVal;
        var suggestions = obj.response.suggestions;
        var length = suggestions.length;
        var currentItem = null;
        var list = '';
        var listHeader = '<li class="text-suggestion suggestion-list">Sugestão de busca</li>';
        var totalList = null;

        for (var i = 0; i < length; i++) {
            currentItem = normalizeValue(suggestions[i]);
            if (currentItem.indexOf(this.currentVal) > -1){
                this.suggestionResult.push(suggestions[i]);
                list += this.setTemplateSuggestion(suggestions[i]);
            }
        }
        if(list){
            totalList = listHeader + list;
            this.resultSuggestion.html(totalList);
        }
    };

    Suggestion.prototype.setTemplateSuggestion = function(item) {
        return templateSuggestion.split('${title}').join(this.setBold(item))
                    .split('${link}').join(item);
    };

    Suggestion.prototype.setBold = function (list) {
        var regExp = new RegExp(this.currentVal, 'ig');
        var replaceMask = '<b>' + this.currentVal + '</b>';
        return list.replace(regExp, replaceMask);
    };

    exports.Suggestion = Suggestion;
});
