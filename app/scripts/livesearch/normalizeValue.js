define(['jquery'], function ($) {
    'use strict';

    return function normalizeValue(val) {
        var valLowCase = val.toLowerCase();
        var textNormalized = $('<div />').html(valLowCase).text();
        return textNormalized;
    };
});
