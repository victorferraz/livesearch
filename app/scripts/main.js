'use strict';

require.config({
    paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
    }
});

require([ 'jquery','livesearch/livesearch'], function ($, livesearch) {

    function init() {
        if ( $('#search').length >0 ){
            new livesearch.Livesearch($('#search'));
        }
    }
    $(init);

});
